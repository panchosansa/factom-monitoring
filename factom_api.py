import json
import requests


class FactomApiException(Exception):
    def __init__(self, message, status_code=None, payload=None):
        super(FactomApiException, self).__init__(message)

        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


def factom_api_post(method, params=None):
    data = {
        'jsonrpc': '2.0',
        'id': 0,
        'method': method
    }
    if params:
        data['params'] = params

    try:
        response = requests.post(
            url='http://localhost:8088/v2',
            data=json.dumps(data),
            headers={'Contenty-Type': 'text/plain'})

        response.raise_for_status()

        res = json.loads(response.content)['result']

        if 'rawdata' in res:
            del res['rawdata']

        return res
    # TODO: Add finer-grained error handling
    except requests.exceptions.HTTPError as e:
        print 'Bad status code for request {}'.format(data), e
        raise FactomApiException(e.message, 400, data)
    except Exception as e:
        print 'Problem with request {}'.format(data), e
        raise FactomApiException(e.message, 400, data)


def get_heights():
    return factom_api_post('heights')


def get_dblock_head():
    return factom_api_post('directory-block-head')


def get_dblock(keymr):
    return factom_api_post('directory-block', {'keymr': keymr})


def get_dblock_by_height(height):
    return factom_api_post('dblock-by-height', {'height': height})


def get_entry_block(keymr):
    return factom_api_post('entry-block', {'keymr': keymr})
