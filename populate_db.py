import pprint
import factom_api as api
import pymongo
import stats
import time

from pymongo import MongoClient


def get_last_processed_block(db):
    last_processed_block = None

    last_entry = db.metrics.find_one(
        sort=[('dbheight', pymongo.DESCENDING)])

    # Last entry will be none when we initially populate the collection as it
    # will be empty
    if last_entry is not None:
        return last_entry['dbheight']
    else:
        return 0


if __name__ == '__main__':

    client = MongoClient()
    db = client["factom"]

    while True:
        cur_height = api.get_heights()['directoryblockheight']
        last_stored_height = get_last_processed_block(db)

        if cur_height > last_stored_height:
            for bh in xrange(last_stored_height + 1, cur_height + 1):
                if bh % 100 == 0:
                    print "Ingesting block {}".format(bh)
                dblock = api.get_dblock_by_height(bh)['dblock']
                ts = dblock['header']['timestamp']
                db["metrics"].insert_one({
                        'wps': stats.wps(bh),
                        'dbheight': bh,
                        'ts': ts
                })

        time.sleep(60)
