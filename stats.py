import factom_api as api


ADMIN_CHAINS = set([
    '000000000000000000000000000000000000000000000000000000000000000a',
    '000000000000000000000000000000000000000000000000000000000000000c',
    '000000000000000000000000000000000000000000000000000000000000000d',
    '000000000000000000000000000000000000000000000000000000000000000f',
])


def wps(height):
    assert height > 0, "Height must be 1 or greater"
    assert int(height) == height, "Height must be an integer"

    total_entries = 0
    dblock = api.get_dblock_by_height(height)['dblock']
    prev_dblock = api.get_dblock_by_height(height - 1)['dblock'] 
    ts = dblock['header']['timestamp']
    prev_ts = prev_dblock['header']['timestamp']
    db_entries = dblock['dbentries']

    for entry in db_entries:
        # Do not include admin chains in the WPS statistic
        if entry['chainid'] not in ADMIN_CHAINS:
            entry_block_details = api.get_entry_block(entry['keymr'])
            total_entries += len(entry_block_details['entrylist'])

    return total_entries / float(ts - prev_ts) / 60
