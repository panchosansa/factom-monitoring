# Factom community testnet monitoring
The app provides the following metrics:

* WPS in each directory block

## Pre-requisites
A Factom node running on the same host as the app with the factomd API port open
(8088).

## Dependencies
The app has a number of dependencies:

* npm
* MongoDB
* pymongo
* requests
* flask

Those need to be installed first. Following this, `cd static` and install the
npm packages required for plotting (`tauCharts`) by running `npm install`.

After installing MongoDB, make sure `mongod` is running on the default port
(27017).

## Running
The data for the metrics is being read from MongoDB (database `factom`,
collection `metrics`). Hence, the first thing to do is to populate the
database. This is done by running the `populate_db.py` script.

NOTE: Leave the script running, as it's continuously polling the Factom node
for new data, computing the WPS for each block as it arrives and storing
the metric in the DB.

Once the database is populated, run `python server.py`. This should expose the
following public endpoints on port 5050:

* `/wps`: return the WPS in the last directory block
* `/wps-history`: return all WPS data in MongoDB
* `/wps-history/<startBlock>`: return the WPS data from `startBlock` (inclusive)
* `/monitoring`: show the monitoring graphs
* `/monitoring/<startBlock>`: show the monitoring graphs from `startBlock` (inclusive)
