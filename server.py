import json
import factom_api as api
import flask
import requests
import stats

from bson import json_util
from flask import Flask, render_template
from pymongo import MongoClient


DEFAULT_START_BLOCK = 11000

app = Flask(__name__, template_folder="./static/html")

@app.errorhandler(api.FactomApiException)
def handle_factom_api_error(error):
    response = flask.json.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/monitoring')
@app.route('/monitoring/<start_block>')
def monitoring(start_block=DEFAULT_START_BLOCK):
    return render_template("monitoring.html", start_block=start_block,
        data=wps_history(start_block))


@app.route('/wps')
def wps():
    height = api.get_heights()['directoryblockheight']
    dblock = api.get_dblock_by_height(height)['dblock']
    return flask.json.jsonify({
            'wps': stats.wps(height),
            'dbheight': height,
            'ts': dblock['header']['timestamp']
    })


@app.route('/wps-history/<start_block>')
def wps_history(start_block):
    end_block = api.get_heights()['directoryblockheight']
    client = MongoClient()
    db = client['factom']
    return json_util.dumps(db.metrics.find({
        'dbheight': {
                     '$gte': int(start_block),
                     '$lte': int(end_block)
                    },
        },
        {'dbheight': 1, 'wps': 1, '_id': 0}
    ))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5050)
